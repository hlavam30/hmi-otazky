# Období osob zmíněných v předmětu HMI

Chronologicky seřazené.

## Antika

- **Tháles z Mílétu**: 640 - 545 př. n. l.
- **Pýthagoras**: 6 st. př. n. l.
- **Aristotelés**: 384 - 322 př. n. l.
- **Platón**: 327 - 347 př. n. l.
- **Eratosthenés z Kyrény**: 3 st. př. n. l.
- **Archimédés**: 287 - 212 př. n. l.
- **Apollónios z Pergy**: kolem 200 př. n. l.
- **Ptólemaios**: 100 - 150 n. l.
- **Diofantos**: 3. st. n. l.

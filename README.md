# Otázky pro self-test z předmětu HMI

[Odpovědi na otázky](answers.md)

## Úvod
- Kdo byl Albrecht Dürer?
- Co to je Čínský abakus?
- Co to jsou Napierovy hůlky?

## Řecko

- Kdo byl Archýtas z Tarentu?
- Co vynalezl Ktésibios?
- Kdo byl Aristotelés, co vynalezl a sepsal?
- Co vynalezl Héron z Alexandie?
- Co vynalezl a sepsal Filón z Byzancie?

## Antika

- Kdo to byl Thalés z Milétu a kdy žil a o co se snažil?
- Popiš Thaletovu větu
- Kdo byl Pýthagorás ze Samu, co dokázal, co objevil, čím se proslavil a kdy žil?
- Kdo a kdy formuloval kvadraturu kruhu? Co to znamená?
- Co to jsou dokonalá čísla?
- Co to jsou spřátelená čísla?
- Kdo byl Platón, kdy žil a čím se proslavil?
- Co to je Eratosthenovo síto, kdo je jeho autorem?
- Co je to odometr?
- Co je to Pickova Věta, kdo je jejím autorem? Uveďte příklad.
- Jaký způsob uvažování zavedl Platón?
- Kterým oblastem matematiky se mají podle Platóna věnovat filosofově, kterí mají vládnout Athénám?
- Na jaké vlastnosti logaritmů je založeno použití logaritmického pravítka?
- Kteří významní autoři se zabývali zhotovováním automatů? Uveďte alespoň dva příklady.
- Základní věta aritmetiky?
- Co to byla Antikythéra, kdy vznikla, kdy byla objevena, k čemu sloužila?

### Eukleidovy Základy

- Kdo byl Eukleidés, kdy žil a čím se proslavil?
- Jak je Eukleidem definován bod a čára?
- Vyslovte pět postulátů (axiomů)
- Co byly Základy, kdo provedl významné překlady?
- Kdy vznikly základy?
- Jeho knihy?
- Popište alespoň dva problémy.
- Co jsou to pravidelná tělesa? Vyjmenujte je. Jaké mají zajímavé vlastnosti?

### Eukleidův algoritmus

- Co je to Eukleidův algoritmus?
- K čemů se EA používá?
- Kdo odhadl vzdálenost měsíce a jak?
- Kdo byl Archimedes ze Syrakus, kdy žil a co vynalezl?
- Pro co Archimedes použil eukleidův algorimus?
- Kdo to byl Isa Al-Mahani, kdy žil?
- Kdo to byl Apollonios z Pergy?
- Co je to Diofantova rovnice, kdo je jejím autorem?
- Co je to Bézoutova identita, kdo ji objevil?
- Co to jsou řetězové zlomky, kde lze nalézt jejich počátek a jaký je jejich vývoj?
- Co to byl Stomachion, kdo je vynálezcem a kdy?
- Co je to bézoutova rovnost a kdo ji prvně použil?
- Mohl Christian Huygens použít Eukleidův algoritmus? Pokud ano, k čemu ho použil.

## Počítače středověku

- Jaký systém vynalezl Béda Ctihodný a k čemu byl určen?
- Kdo byl Gerbert z Aurillacu a co vynalezl?
- Co je to metoda gelosia?
- Kdo je autorem Napierových hůlek?
- Kteří lidé stojí za mechanizací myšlení a jak?
- Co je to astroláb? Z jakých částí se skládá? K čemu se používá?
- Kdo to byl Thabit ibn Qurra, kdy žil a čím se proslavil? Jaký mají tvar jeho prvočísla?
- Mikoláš Kopernik?
- Kdo se pokoušel o středověký počítač a kdo jej reálně stvořil a co počítač počítal?

## Počátky variačního počtu

- Co to je variační počet?
- Kdo byl Nicole Oresme a čím se proslavil?
- Co to byla Mertonova kolej v Oxfordu?
- Co je to Fermatův princip, kdo je jeho autorem?
- Co je to Fermatovo číslo? Co to jsou pseudoprvočísla? Jsou všechna Fermatova čísla prvočísla?
- Co to jsou Fermatovy věty?
- Co je to Fermatův bod?
- Kdo je René Descartes a kdy žil?
- Kdo se dále věnoval variačnímu počtu?
- Co je to GEM a kdo je její autorem?
- Metoda výpočtu Velikonoc?
- Vyjmenujte všechny Gaussovy důležité objevy.
- Co to jsou transcendentní křivky a kdo se jimi zabýval? Jaké to jsou?

## Matematika pražského orloje

- O jakou projekci se v případě pražského orloje jedná?
- Jaké jsou části pražského orloje?
- Popište vlastnosti pražského orloje
- Kdo je považován za autora matematického modelu orloje?
- Kdo je autorem, čím byl a kdy žil?
- Co je to Šindelovská posloupnost?
- Jaké časy lze číst na orloji? Jak jsou zavedeny?
- Co je stereografická projekce, kde se využívá?

## Matematika v Číně

- Čím se Číňané zabývali?
- Které matematické objevy pocházejí z Číny?

## Matematika 16. -- 17. století

- Čím se proslavil Gerolamo Cardano?
- Kdo byl Francois Viete, kdy žil a co objevil?
- Kdo byl Simon Stevin?
- Jaké znamenité počítače vznikly v této době?
- Čím se proslavil Charles Babbage?
- ENIAC?
- Analytic Engine? Popište jeho strukturu a dobu vzniku.
- Uveďte alespoň 3 tvůrce mechanických kalkulátorů.
- Kdo sestrojil počítací hodiny, kdy a komu byly určeny?
- Napište alespon dvě jména československých tvůrců počítačů. Kdy žili, něco o životě.
- Kdo využil děrných štítků pro počítače?
- Jakou vlastnost logaritmu využívá logaritmické pravítko?

## Matematika 18. -- 19. století

- Kdo byl Bernard Bolzano, čím se proslavil a kdy žil?
- Kdo byl autorem první české učebnice algebry?
- Kdo byl Fraz Joseph Gerstner?
- Napište alespoň 3 Booleovy vynálezy.

## Matematika 19. století

- Co bylo příčinou nutnosti kryptografie?
- Kdo je to Augustus de Morgan, kdy žil a čím se proslavil?


## Matematika 20. a 21. století

- Vyjmenujte některé problémy 20. století. Které jsou vyřešené a které ne?
- Vyjmenujte některé problémy pro 21. století.

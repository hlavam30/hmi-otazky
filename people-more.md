# Krátké výpisy k osobám zmíněným v předmětu HMI

## Albrecht Dürer
 - zabýval se geometrickou konstrukcí písem
 - dal základ nápisům na budovách a sázení knih

## Archýtas z Tarentu
 - nejstarší známý konstruktér automatu
 - sestrojil holubici, která sama létala

## Ktésibios
 - jeden z prvních tvůrců automatů
 - vodní hodiny, hasičské stříkačky

## Aristotelés ze Stageiry
 - 384 př. n. l., řecký filosof
 - Platonův žák
 - ozubená kola, spis Organón o logice

## Héron z Alexandrie
 - automatické dveře, odometr, komentoval Základy

## Filón z Byzancie
 - automat pro mytí rukou
 - knihy O automatech a Pneumatika
 - řetězový pohyb

## Tháles z Milétu
 - shana o racionální popis světa
 - Thaletova věta, Thaletova kružnice

## Pýthagorás ze Samu
 - 6. stol. př. n. l.
 - součet úhlů v trojúhelníků je roven dvěma pravým úhlům
 - objevil iracionální čísla
 - 5 pravidelných těles

## Anaxagoras
 - 5. stol. př. n. l.
 - řešil úlohu kvadraturu kruhu

## Carl F. von Lindemann
 - dokázal v roce 1882, že pi je transcendentní

## Platón
 - založil akademii
 - systematizoval matematické myšlení
 - 5 pravidelných těles
 - doporučoval věnovat se stereometrii

## Eratosthenés z Kyrény
 - Eratosthenovo síto

## Georg Alexander Pick
 - Pickova věta - obsah obrazce v mřížce

## Euklediés
 - 4-3 st. př. n. l.
 - napsal Základy
 - vynalezl Euklidův algoritmus

## Adelard z Bathu
 - překladatel Euklidovo Základů

## Gerard z Cremony
 - překladatel Euklidovo Základů
 - překladatel Ptolemaiovo Almagestu

## Oliver Byrne
 - autor grafické verze Euklidovo Základů

## Aristarchos ze Samu
 - odhad vzdálenosti Měsíce
 - tvůrce heliocentrického modelu

## Archimedes ze Syrakus
 - měřil obvodu kruhu vepisováním a opisováním mnohoúhelníků
 - první významné měření
 - významné fyzické zákony
 - hra Stomachion

## Isa Al-Mahani
 - komentoval Archimeda i Euklida
 - pokoušel se řešit Archimédův problém

## Apollonios z Pergy
 - dílo Kónika -- o kuželosečkách

## Diofantos z Alexandrie
 - spis Aritmetika
 - Diofantické rovnice

## Bachet de Méziriac
 - objevil Bézoutovu identitu

## John Wallis
 - symbol nekonečna
 - kryptografie proměnným klíčem, ne složitým algoritmem

## Gerbert z Aurillacu
 - do Evropy přinels gobar a abakus
 - vynalezl kyvadlové hodiny

## John Napier
 - Napierovy hůlky (Rabdologiae)
 - objevy logaritmů

## Ramón Llul
 - myšlenkové logické kombinace
 - mechanizace myšlení
 - ovlivnil Leibnize

## Thabit ibn Qurra
 - revidoval překlad Euklidovo základů
 - překladatel části prací Archiméda
 - překlad Kóniky

## Mikoláš Kopernik
 - známý heliocentrista

## Wilhelm Schickard
 - sestrojil počítací hodiny

## Pierre de Fermat
 - fermatův princip
 - fermatův bod
 - fermatovo číslo
 - fermatovy věty

## René Descartes
 - mechanické křivky:
   - tautochrona, brachistochrona

## Carl Friedrich Gauss
 - GEM, gaussova křivka, normální rozdělení

## Jan Šindel
 - autor pražského orloje

## Gerolamo Cardano
 - dílo Ars magna a v něm řešení bikvadratické převedením na kubickou

## Francois Viete
 - zavedl symboliku pro známé a neznámé
 - trigonometrické vzorce

## Simon Stevin
 - šedesátkový systém nahradil desetinným systémem

## Charles Babbage
 - pracoval na "Analytic Engine" - programovatelný počítač
 - vynalezl rychloměr, kravoplaš
 - prolomil Vigenérovu šifru

## Bernard Bolzano
 - Bolzanova funkce bez derivace
 - prohrál konkurs profesora matematiky
 - spis Vědosloví - moderní formální logika
 - věta o překročení řeky (funkce musela projít 0)

## Stanislav Vydra
 - autor první české učebnice algebry

## Franz Joseph Gerstner
 - zakladatel pražské polytechniky
 - Bolzanův učitel
 - teorie vln

## Augustus de Morgan
 - zakladatel Londýnské matematické společnosti
 - uvedl metodu matematické indukce
 - DeMorganovy zákony
 - inspiroval Adu Byronovou

## David Hilbert
 - Základy geometrie
 - hilbertovy problémy
 - Hilbertova křivka (vyplňování prostoru)

## Leibniz
 - program na reformu logiky - univerzální systém znaků
 - "Není hodno znamenitého člověka..."

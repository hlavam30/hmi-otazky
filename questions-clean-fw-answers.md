- K čemu slouží Pickova věta? Napište ji a uveďtě příklad. Kdo ji vyslovil?
  - Obsah útvarů v čtvercové síti: P(x) = I + B/2 - 1, kde I je počet vnitřních síťových bodů, B je počet bodů na obvodu obrazce

- Mechanické kalkulátory v 17. století, napiště alespoň 3 tvůrce. Kdo sestrojil počítačí hodiny, kdy a komu byly určeny.
  - Wilhelm Schickard, Blaise Pascal, Gottfried Wilhelm von Leibniz

- Co je Stomachion, kdo to vynalezl a kdy.
  - hra skládající se ze 14 dílů ve čtverci, jejíž cílem je z dílů vytvořit různé tvary, např. slona
  - 3 století př. n. l., Archimedés

- Napiště alespoň 3 Booleovy vynálezy.
  - Booleova algebra

- Napiště alespon dvě jména československých tvůrců počítačů. Kdy žili, něco o životě.
  - Antonín Svoboda a Vladimír Vand

- Kdo využil děrných štítků, pro sestavený počítače.
  - Charles Babbage

- Z jakých částí se skládá pražský orloj? Kdo je považován za autora jeho matematického modelu?
  - Jan Šindel
  - jicí stroj, ukazovací, diferenční, bicí, zvonící a kalendářní stroje

- Arabský matematik Avicenna (10.–11. století) ve své „Aritmetice“ napsal: Jestliže číslo vydělíte 9, pak dostanete zbytek 1 nebo 8. Kvadrát téhož čísla po vydělení 9 dá zbytek 1. Platí to? Zkuste tvrzení dokázat. Ověřte tuto vlastnost na třech příkladech.
  - není to pravda

- Kdy vznikly Euklidovy základy? Popište alespoň 2 problémy. Popište Euklidův algoritmus, k čemu se používá? Nalezněte pomocí Eukleidova algoritmu NSD (zadána 2 čísla).
  - konec 4. st. př. n. l.
  - o pravidelných tělesech, o stereometrii, o teorii čísel, o geometrii
  - algoritmus k hledání největšího společného dělitele

- Jaký způsob uvažování zavedl Platon? Kdy žil? Jak odůvodňoval důležitost matematického vzdělání? Kterým oblastem matematiky se mají podle Platóna věnovat filosofově, kterí mají vládnout Athénám
  - systematizoval způsob uvažování DVD - definice, věta, důkaz
  - žil v 4-5 st. n. l.
  - doporučoval zabývat se iracionálními čísly a stereografii

- Na jaké vlastnosti logaritmů je založeno použití logaritmického pravítka?
  - logaritmus součinu je roven součtu logaritmů činitelů

- Definujte Bezoutovu identitu? K čemu se používá? Kdo a kdy ji objevil? Kde a kdy ji vysvětluje Étienne Bézout?
  - gcd(a,b) = xa + yb
  - v 17 st. n. l. objevil Bachet de Meziriac

- Kolikamístná čísla měl zpracovávat Babbageův poslední návrh počítače „Analytic Engine“? Popiště strukturu tohoto Babageova stroje? Kupř. měl paměť? Uveďte dobu, kdy se Babbage projektu věnoval?
  - 50 desetinných míst, měl paměť + procesor a by poháněn parou
  - 19. století

- Uveďte, jaké časy lze číst na pražském orloji. Vyvětlete, jak jsou zavedeny.
  - staročeský (italský), německý a babylónský

- Popište tradiční úlohu nazvanou kvadratura kruhu. Kteří antičtí matematikové ji řešili? Co vítě o její řešitelnosti?
  - hledání čtverce se stejným obsahem jako daný kruh, pomocí pravítka a kružítka
  - Anaxagoras

- Kteří významní autoři antiky se zabývali zhotovováním automatů? Uveďte příklady alespoň dva příklady. Co je to odometr?
  - Herón z Alexandrie, Filón z Byzancie
  - vodní automaty, hasičské stříkačky, parní automatické dveře
  - odometr je měřič vzdálenosti

- Kde se nejdříve objevil Euklidův algoritmus prakticky a teoreticky? Popište jej a ukažte jeho použití na příkladu
  - Aristarchos ze Samu při měření vzdálenosti Slunce a měsíce
  - Teoreticky až Eukleidés

- Fermatův/Torricelliho bod - co to je, jak se konstruuje
  - bod trojúhelníků jehož součet vzdáleností od vrcholů je minimální

- Thabit ibn Qurra - čemu se věnoval, jaký tvar mají Thabitova prvočísla, jsou 11, 23, 47 Thabitova prvočísla?
  - zobecnil Pyt. větu
  - překladatel významných děl (Archimedova, Apollonia, Almagest)
  - 3 * 2^n - 1

- Základní věta aritmetiky
  - Každé přirozené číslo větší než 1 se dá rozepsat jako jednoznačný součin prvočísel.

- Ciferný součet - k čemu se používá, existují čísla rovnající se svému cifernému součtu, napište všechna trojciferná čísla s ciferným součtem 3.
  - ke zjištění dělitelnosti

- Euklidův algoritmus - co to je, jak funguje a první praktické použití
  - algoritmus k hledání největšího společného dělitele

- Planetka Ceres - kdy a jak Gauss spočetl její dráhu.
  - 1801, GEM a první použití metody nejmenších čtverců

- Co to byla Antikythéra, kdy vznikla, kdy byla objevena, k čemu sloužila?
  - analogový počítač, 150 př. n. l.
  - pro výpočet pohybu slunce a měsíce

- Napište tvar Fermatova čísla. Jsou všechna Fermatova čísla prvočísla? Co jsou to pseudoprvočísla?
  - 2^2^n + 1
  - nejsou
  - taková složená čísla, která běžnými testy se jeví na první pohled jako prvočíslo

- Co jsou to pravidelná tělesa? Vyjmenujte je. Jaké mají zajímavé vlastnosti?
  - taková tělesa, jejichž stěny jsou všechny stejné
  - čtyřsten, krychle, osmistěn, dvanáctistěn, dvacetistěn7
  - tetrahedron, krychle, octahedron, dodecahedron, icosahedron

- Ramon Llull bývá pokládán za zakladatele informatiky. Koho ovlivnil? Jaký stroj vytvořil?
  - 13. století objevil logický přístroj pro rozhodování pravdivosti
  - Giordano Bruno, Leibniz

- Mohl Christian Huygens použít Eukleidův algoritmus? Pokud ano, k čemu ho použil.
  - Mohl

- Čím přispěl Gauss + Gaussova eliminační metoda
  - teorie čísel, výpočet data Velké noci
  - gaussova křivka, gaussovo rozdělení, GEM

- Něco o Claude Gaspard Bachet de Méziriac - jeho dílo a koho ovlivnil.
  - jako první se zabýval řešením neurčitých rovnic pomocí řetězových zlomků
  - autor Bézoutovy identity
  - ovlivnil Fermata

- Mersennova čísla + jestli je Mersennovo číslo pro 11 prvočíslo.
  - čísla ve tvaru  2^n - 1
  - 2^{11} - 1 není prvočíslo.

- Co je to astroláb, z jakých částí se skládá, k čemu se používá?
  - přístroj k zjištění východu/západu slunce, poloh nebeských těles atp.
  - skládá se ze základní desky, výměnných desek, sítě, koníka a zaměřovacím pravítkem

- „Není hodno znamenitého člověka trávit čas výpočty jako otrok.“ - kdo vyslovil tento citát? Čím se tento člověk zabýval?
  - Leibniz

- Co je stereografická projekce, kde se využívá?
  - promítání kulové plochy na rovinu
  - pražský orloj

- Brachistochrona - Co to je, kdo se zabýval touto problematikou?
  - mechanická křivka
  - Bernoulli

- Známý helénský učenec Archimédes zahynul ve věku 75 let v roce 212 př. n. l. Byl zabit římským vojákem v době, kdy Syrakusy byly obléhány Římany. Určete rok jeho narození. Jaké výročí jeho narození bychom letos mohli oslavit? Uveďte a popište alespoň jeho 3 objevy.
  - 287 př. n. l.
  - 2304.
  - Stomachion, první důsledný odhad pi, archimedovy fyzické zákony
